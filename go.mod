module gitlab.com/tjackiw/arangomigo

require (
	github.com/arangodb/go-driver v0.0.0-20191028094806-4407cf842de1
	github.com/pkg/errors v0.8.0
	github.com/stretchr/testify v1.2.2
	gopkg.in/yaml.v2 v2.2.1
)

go 1.13
